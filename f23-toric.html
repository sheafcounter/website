<!doctype html>
<html xmlns=http://www.w3.org/1999/xhtml lang xml:lang>
    <head>
    <meta charset=utf-8>
    <meta name=viewport content="width=device-width,initial-scale=1,user-scalable=yes">
    <title>Undergraduate seminar on toric varieties</title>
    <link rel=stylesheet href=air.css>
    <script type="text/javascript"
        src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
    </script>
    </head>
    <body>
    <header>
        <h4><a class="head" href=index.html>Patrick Lei</a></h4>
        <nav><ul>
                <li><a class="head" href=research.html>Research</a>
                <li><a class="head" href=notes.html>Notes</a>
                <li><a class="head" href=teaching.html>Teaching</a>
                    <li><a class="head" href=seminars.html>Seminars</a></ul></nav></header>

	    <h5>Toric Varieties (Fall 2023)</h5>
	    <ul>
		<li>Organizer: <a href="index.html">Patrick Lei</a></li>
		<li>When: Wednesdays, 6-8 PM</li>
		<li>Where: Math 528</li>
	    </ul>
	    <p>References include:</p>
	    <ul>
		<li><strong>[F]</strong> William Fulton, <i>Introduction to toric varieties.</i> Short and sweet. We will follow the order of topics in this book, but you can use <strong>[CLS]</strong> or <strong>[O]</strong> if you prefer their exposition.</li>
		<li><strong>[CLS]</strong> David Cox, John Little, and Hal Schenck, <i>Toric varieties</i> This is the newest, longest (by far), and most elementary.</li>
		<li><strong>[O]</strong> Tadao Oda, <i>Convex bodies and algebraic geometry</i> This is the oldest and most technical book, written by an expert in the field.</li>
	    </ul>
	    
	    <h5>About this seminar</h5>
	    <p>Toric varieties are special geometric objects which are defined using combinatorial information. This combinatorial data can be used to compute many geometric properties of toric varieties. Even though toric varieties are quite special, many general phenomena can be observed on them. In this semester, we will discuss the basic geometry of toric varieties and then see their applications in algebraic geometry and other areas of mathematics. For example, applications of toric varieties to results in combinatorics are discussed in Chapter 5 of <strong>[F]</strong> and in Chapter 4 of <strong>[O]</strong>.</p>
	    <h5>Expectations</h5>
	    <p>Each participant will give at least one talk over the course of the semester, during which I hope you enjoy some interesting mathematics and improve your presentation skills. Speakers are required to meet with me once at least 24 hours before your talk, at which point you should give me a title and abstract. After your talk, please email me a copy of your notes. When you are not speaking, I hope that you can help form a friendly and lively seminar environment. The expectations are as follows:</p>
	    <ul>
		<li>Attendance is required. If you must miss class for some reason, please email me as soon as possible.</li>
		<li>For every talk in which you are not speaking, please write <a href="https://math.stanford.edu/~vakil/threethings.html">three things</a> (read the link to see what a "thing" is) on a sheet of paper (with your name and date on it) and give it to me at the end of the class. Note that you may write the three "things" during the talk.</li>
		<li>Do not be afraid to ask questions during the talk.</li>
	    </ul>
	    <h5>Outline of the seminar</h5>
	    <p>Fundamental topics:</p>
	    <ul>
		<li>Crash course on algebraic varieties</li>
		<li>Polyhedral cones. <i>Reference:</i> <strong>[F]</strong>, section 1.2</li>
		<li>Affine toric varieties. <i>Reference:</i> <strong>[F]</strong>, section 1.3</li>
		<li>Fans and general toric varieties. <i>Reference:</i> <strong>[F]</strong>, section 1.4</li>
		<li>Toric varieties from polytopes. <i>Reference:</i> <strong>[F]</strong>, section 1.5</li>
		<li>Local properties. <i>Reference:</i> <strong>[F]</strong>, section 2.1. Ignore the stuff about toric varieties being Cohen-Macaulay -- this is a technical condition in commutative algebra.</li>
		<li>Toric surfaces. <i>Reference:</i> <strong>[F]</strong>, section 2.2.</li>
		<li>1-parameter subgroups and limit points, compactness. <i>Reference:</i> <strong>[F]</strong>, sections 2.3, 2.4.</li>
		<li>Smooth toric surfaces. <i>Reference:</i> <strong>[F]</strong>, section 2.5</li>
		<li>Toric resolution of singularities. <i>Reference:</i> <strong>[F]</strong>, section 2.6; <strong>[CLS]</strong>, sections 10.1, 11.1.</li>
		<li>Orbits of the torus action. <i>Reference:</i> <strong>[F]</strong>, section 3.1</li>
		<li>Topological properties. <i>Reference:</i> <strong>[F]</strong>, section 3.2</li>
		<li>Divisors and line bundles. <i>Reference:</i> <strong>[F]</strong>, sections 3.3, 3.4</li>
		<li>Tangent bundle, canonical divisor. <i>Reference:</i> <strong>[F]</strong>, section 4.3; <strong>[CLS]</strong>, chapter 8.</li>
	    </ul>
	    <p>More advanced topics you may choose to talk about if you are interested, listed roughly in order of increasing difficulty. Note that some of these will take several talks. You can also choose a topic not on this list if you want.</p>
	    <ul>
		<li>A compact but not projective toric variety. <i>Reference:</i>  <strong>[CLS]</strong>, example 6.3.26.</li>
		<li>Applications to combinatorics. <i>Reference:</i> <strong>[F]</strong>, sections 5.3, 5.4, 5.6, <strong>[CLS]</strong>, section 10.5</li>
		<li>Toric varieties as quotients and the secondary fan. <i>Reference:</i> <strong>[CLS]</strong>, sections 5.0, 5.1, chapters 14, 15.</li>
		<li>Reflexive polytopes and Fano toric varieties. <i>Reference:</i> <strong>[CLS]</strong>, section 5.1, chapters 14, 15.</li>
		<li>Application of the above to mathematical physics. <i>Reference:</i> Victor Batyrev, <i><a href="https://arxiv.org/abs/alg-geom/9310003">Dual polyhedra and mirror symmetry for Calabi-Yau hypersurfaces in toric varieties.</a></i></li>
	    </ul>
	    <h5>Schedule</h5>
	    <p>Each talk will last approximately 50 minutes. The schedule is subject to change at any point.</p>
	    <dl>
		<dt>9/13</dt>
		<dd>Patrick Lei</dd>
		<dd><strong>Crash course on algebraic varieties</strong></dd>
		<dd>I will tell you about algebraic varieties, rings, semigroups, and various other notions we will need for this seminar. We will also do organizational stuff.</dd>
		<dd><a href="docs/toric/toric1.pdf">Notes</a></dd>
		<dt>9/20</dt>
		<dd><del>Charlotte Coats</del></dd>
		<dd><strong><del>Rational polyhedral cones</del></strong></dd>
		<dd>Thomas Bueler-Faudree</dd>
		<dd><strong>Affine toric varieties</strong></dd>
		<dd><a href="docs/toric/thomas-1.pdf">Notes</a></dd>
		<dt>9/27</dt>
		<dd>Charlotte Coats</dd>
		<dd><strong>Rational polyhedral cones</strong></dd>
		<dd><a href="docs/toric/charlotte-1.pdf">Notes</a></dd>
		<dd>Rahul Ram</dd>
		<dd><strong>Fans and general toric varieties</strong></dd>
		<dd><a href="docs/toric/rahul-1.pdf">Notes</a></dd>
		<dt>10/4</dt>
		<dd>Peng Liu</dd>
		<dd><strong>Toric varieties from polytopes</strong></dd>
		<dd><a href="docs/toric/peng-1.pdf">Notes</a></dd>
		<dd>William Durie</dd>
		<dd><strong>Local properties</strong></dd>
		<dd><a href="docs/toric/william-1.pdf">Notes</a></dd>
		<dt>10/11</dt>
		<dd>Patrick Lei<dd>
		<dd><strong>Crash course 2</strong></dd>
		<dd><a href="docs/toric/toric2.pdf">Notes</a></dd>
		<dd>Jane Meenaghan<dd>
		<dd><strong>Toric surfaces</strong></dd>
		<dd><a href="docs/toric/jane-1.pdf">Notes</a></dd>
		<dt>10/18</dt>
		<dd>Casey Qi<dd>
		<dd><strong>1-parameter subgroups, limit points</strong></dd>
		<dd><a href="docs/toric/casey-1.pdf">Notes</a></dd>
		<dd>Jake Bernstein<dd>
		<dd><strong>Smooth toric surfaces</strong></dd>
		<dd><a href="docs/toric/jake-1.pdf">Notes</a></dd>
		<dt>10/25</dt>
		<dd>Kevin Hernandez<dd>
		<dd><strong>Toric resolution of surface singularities</strong></dd>
		<dd><a href="docs/toric/kevin-1.pdf">Notes</a></dd>
		<dd>Jennifer Luo<dd>
		<dd><strong>Toric resolution of singularities</strong></dd>
		<dd><a href="docs/toric/jennifer-1.pdf">Notes</a></dd>
		<dt>11/1</dt>
		<dd>No Seminar<dd>
		<dt>11/8</dt>
		<dd>Lilah Li<dd>
		<dd><strong>Orbits</strong></dd>
		<dd><a href="docs/toric/lilah-1.pdf">Notes</a></dd>
		<dd>Rahul Ram<dd>
		<dd><strong>Divisors</strong></dd>
		<dd><a href="docs/toric/rahul-2.pdf">Notes</a></dd>
		<dt>11/15</dt>
		<dd>Peng Liu<dd>
		<dd><strong>Line bundles 1</strong></dd>
		<dd><a href="docs/toric/peng-2.pdf">Notes</a></dd>
		<dd>Jake Bernstein<dd>
		<dd><strong>Line bundles 2</strong></dd>
		<dd><a href="docs/toric/jake-2.pdf">Notes</a></dd>
		<dt>11/22</dt>
		<dd>No Seminar (Thanksgiving)<dd>
		<dt>11/29</dt>
		<dd>Henny Kim<dd>
		<dd><strong>Moment map and the polytope</strong></dd>
		<dd><a href="docs/toric/henny-1.pdf">Notes</a></dd>
		<dd>Jennifer Luo<dd>
		<dd><strong>Reflexive polytopes and Fano toric varieties</strong></dd>
		<dd><a href="docs/toric/jennifer-2.pdf">Notes</a></dd>
		<dt>12/6</dt>
		<dd>Patrick Lei<dd>
		<dd><strong>Reflexive polytopes and mirror symmetry</strong></dd>
		<dd>I will explain an application of reflexive polytopes and Fano toric varieties to mathematical physics.</dd>
        <dd><i>Reference: <a href="https://www.mdpi.com/2073-8994/7/3/1633">Cox</a>, <a href="https://arxiv.org/abs/alg-geom/9310003">Batyrev</a></i></dd>
		<dd><a href="docs/toric/toric3.pdf">Notes</a></dd>
	    </dl>
    </body>
</html>
